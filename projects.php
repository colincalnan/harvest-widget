<?php
require_once( dirname(__FILE__) . '/config.php' );

if ($client_id = $_GET['cid']) {
// Build a list of Projects related to a client
	$result = $api->getClientProjects($client_id);
	if($result->isSuccess()) {
		$projects = $result->data;
		foreach ($projects as $project) {
			if($project->get("active")) {
				$project_list[] = array(
					'optionValue' => $project->get("id"),
					'optionDisplay' => $project->get("name")
				);
				//$project_list[$project->get("id")] = $project->get("name");
			}
		}
	}
echo json_encode($project_list);
//print_r("Getting Projects.<br />");
}