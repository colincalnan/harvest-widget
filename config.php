<?php

require_once( dirname(__FILE__) . '/harvest-api/HarvestAPI.php' );
spl_autoload_register(array('HarvestAPI', 'autoload'));

// Set account and user info
$api = new HarvestAPI();
$api->setUser( "colin@raisedeyebrow.com" );
$api->setPassword( "Guitar&Bass" );
$api->setAccount( "reyebrow" );

$api->setRetryMode( HarvestAPI::RETRY );
$api->setSSL(true);