<?php
require_once( dirname(__FILE__) . '/config.php' );

if ($project_id = $_GET['pid']) {
	// Get the number of hours completed for a specific project
	$range = new Harvest_Range('20120101', date('Ymd'));
	$range = $range->thisMonth();
	  
	$result = $api->getProjectEntries( $project_id, $range);
	if( $result->isSuccess() ) {
	  $dayEntries = $result->data;
	  $budget_spent =  0;
	  foreach($dayEntries as $entry) {
	  	if($entry->get("task-id") == 58320) {
	  		$budget_spent = $budget_spent + $entry->get("hours");
	  	}
	  }
	  $return['budget_spent'] = $budget_spent;
	}

	// Get the total budget for a specific project
	$result = $api->getProjectTaskAssignments( $project_id ); 
	if( $result->isSuccess() ) { 
		$taskAssignments = $result->data;
		foreach ($taskAssignments as $task) {
		 	if($task->get("task-id") == 58320) {
		 		$budgeted_total	= $task->get("budget");
		 		break;
		 	}
		} 
	 	$return['budgeted_total'] = $budgeted_total;
	} 

	$return['budget_left_hours'] = $budgeted_total - $budget_spent;
	$return['budget_left_percentage'] = floor(100 - (($budget_spent / $budgeted_total) * 100));

	print json_encode($return);
}
 // 58320 is the Task ID for Build out...