;(function ($, window, undefined) {
  'use strict';

  var $doc = $(document),
      Modernizr = window.Modernizr;

  $(document).ready(function() {
    $.fn.foundationAlerts           ? $doc.foundationAlerts() : null;
    $.fn.foundationButtons          ? $doc.foundationButtons() : null;
    $.fn.foundationAccordion        ? $doc.foundationAccordion() : null;
    $.fn.foundationNavigation       ? $doc.foundationNavigation() : null;
    $.fn.foundationTopBar           ? $doc.foundationTopBar() : null;
    $.fn.foundationCustomForms      ? $doc.foundationCustomForms() : null;
    $.fn.foundationMediaQueryViewer ? $doc.foundationMediaQueryViewer() : null;
    $.fn.foundationTabs             ? $doc.foundationTabs({callback : $.foundation.customForms.appendCustomMarkup}) : null;
    $.fn.foundationTooltips         ? $doc.foundationTooltips() : null;
    $.fn.foundationMagellan         ? $doc.foundationMagellan() : null;
    $.fn.foundationClearing         ? $doc.foundationClearing() : null;

    $.fn.placeholder                ? $('input, textarea').placeholder() : null;
  });

  // UNCOMMENT THE LINE YOU WANT BELOW IF YOU WANT IE8 SUPPORT AND ARE USING .block-grids
  // $('.block-grid.two-up>li:nth-child(2n+1)').css({clear: 'both'});
  // $('.block-grid.three-up>li:nth-child(3n+1)').css({clear: 'both'});
  // $('.block-grid.four-up>li:nth-child(4n+1)').css({clear: 'both'});
  // $('.block-grid.five-up>li:nth-child(5n+1)').css({clear: 'both'});

  // Hide address bar on mobile devices (except if #hash present, so we don't mess up deep linking).
  if (Modernizr.touch && !window.location.hash) {
    $(window).load(function () {
      setTimeout(function () {
        // At load, if user hasn't scrolled more than 20px or so...
  			if( $(window).scrollTop() < 20 ) {
          window.scrollTo(0, 1);
        }
      }, 0);
    });
  }  

  // Set up Chosen selects
  $(".chosen").chosen();
   // Ajax functioality for Client dropdown
  $("#client").chosen().change(function(){
    $.getJSON("/projects.php",{cid: $(this).val()}, function(j){
      var options = '';
      for (var i = 0; i < j.length; i++) {
        options += '<option value="' + j[i].optionValue + '">' + j[i].optionDisplay + '</option>';
      }
      $("#project").html(options);
      $('#project option:first').attr('selected', 'selected');
      $("#project").trigger("liszt:updated");
    })
  }); 

  // Ajax functionality for Project dropdown
  $("#project").chosen().change(function(){
    // Call tasks.php
    $.getJSON("/tasks.php",{pid: $(this).val()}, function(j){
      // Set all the values that are returned
      $(".budgeted-total").html(j.budgeted_total + ' hours');
      $(".budget-spent").html(j.budget_spent + ' hours');
      $(".budget-left").html(j.budget_left_hours + ' hours (' + j.budget_left_percentage + '%)');
      // Update the progress bar
      var progressbar = $( ".progressbar" );
      progressbar.progressbar('value', 100 - j.budget_left_percentage);
      $("#project").trigger("liszt:updated");
    })
  });   

  // Draggable element
  $( ".draggable" ).draggable({ axis: "x", containment: "parent" });
 
  // Progress bar - Budget indicator
  var progressbar = $( ".progressbar" );
  progressbar.progressbar({
    value: parseFloat(progressbar.attr('data-value'))
  });
  var progressbarValue = progressbar.find( ".ui-progressbar-value" );
  progressbarValue.css({
    "background": '#41b419'
  });


})(jQuery, this);
