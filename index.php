<?php
require_once( dirname(__FILE__) . '/config.php' );

// Build a list of Clients
$result = $api->getClients();
if( $result->isSuccess() ) {
	$clients = $result->data;
	foreach ($clients as $client) {
		if($client->get("active")) {
			$client_list[$client->get("id")] = $client->get("name");
		}
	}
//print("Getting Clients.<br />");
}

?>
<!DOCTYPE html>

<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if IE 8]> <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->

<head>
	<meta charset="utf-8" />

	<!-- Set the viewport width to device width for mobile -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<title>Harvest Widgets</title>
	<!-- Included CSS Files -->
	<link rel="stylesheet" href="foundation/stylesheets/app.css">
	<link rel="stylesheet" href="chosen/chosen/chosen.css">
	<link rel="stylesheet" href="jquery-ui/css/smoothness/jquery-ui-1.10.0.custom.min.css">
	<script src="foundation/javascripts/foundation/modernizr.foundation.js"></script>
</head>
<body>
	<div class="row">
		<div class="four columns draggable widget" id="">
			<h3><?php //print $project->get("name"); ?></h3>
			<form action="<?php $_SERVER['PHP_SELF']; ?>" id="widget-form" method="POST">
				<select id="client" name="client-id" class="chosen">
				<?php	
				foreach ($client_list as $key => $value) {
					if($_POST['client-id'] == $key) {
						$selected = "selected";
					} else {
						$selected = '';
					}
					print '<option value="'.$key.'" ' . $selected . '>'.$value.'</option>';
				}
				?></select>
				<?php //if($project_list): ?>
					<select id="project" name="project-id" class="chosen">
					<?php
					/*foreach ($project_list as $key => $value) {
						if($_POST['project-id'] == $key) {
							$selected = "selected";
						}
						print '<option value="'.$key.'"' . $selected . '>'.$value.'</option>';
					}*/
					?></select>
				<?php //endif; ?>
			</form>
			<div class="row">
			  	<div class="four columns"><label>Budget:</label> <span class="budgeted-total"></span></div>
			  	<div class="four columns"><label>Spent:</label> <span class="budget-spent"></span></div>
			  	<div class="four columns"><label>Budget Left: </label><span class="budget-left "></span></div>
			</div>
			<div class="row">
				<div class="progressbar" data-value=""></div>
			</div>
		</div>
	</div>

	<!-- Included JS Files (Uncompressed) -->
	<script src="foundation/javascripts/foundation/jquery.js"></script>
	<script src="foundation/javascripts/foundation/jquery.cookie.js"></script>
	<script src="foundation/javascripts/foundation/jquery.event.move.js"></script>
	<script src="foundation/javascripts/foundation/jquery.event.swipe.js"></script>
	<script src="foundation/javascripts/foundation/jquery.foundation.accordion.js"></script>
	<script src="foundation/javascripts/foundation/jquery.foundation.alerts.js"></script>
	<script src="foundation/javascripts/foundation/jquery.foundation.buttons.js"></script>
	<script src="foundation/javascripts/foundation/jquery.foundation.clearing.js"></script>
	<script src="foundation/javascripts/foundation/jquery.foundation.forms.js"></script>
	<script src="foundation/javascripts/foundation/jquery.foundation.joyride.js"></script>
	<script src="foundation/javascripts/foundation/jquery.foundation.magellan.js"></script>
	<script src="foundation/javascripts/foundation/jquery.foundation.mediaQueryToggle.js"></script>
	<script src="foundation/javascripts/foundation/jquery.foundation.navigation.js"></script>
	<script src="foundation/javascripts/foundation/jquery.foundation.orbit.js"></script>
	<script src="foundation/javascripts/foundation/jquery.foundation.reveal.js"></script>
	<script src="foundation/javascripts/foundation/jquery.foundation.tabs.js"></script>
	<script src="foundation/javascripts/foundation/jquery.foundation.tooltips.js"></script>
	<script src="foundation/javascripts/foundation/jquery.foundation.topbar.js"></script>
	<script src="foundation/javascripts/foundation/jquery.placeholder.js"></script>
	
	<script src="chosen/chosen/chosen.jquery.min.js"></script>

    <script src="http://malsup.github.com/jquery.form.js"></script> 

	<script src="jquery-ui/js/jquery-ui-1.10.0.custom.min.js"></script>

  	<!-- Application Javascript, safe to override -->
  	<script src="foundation/javascripts/foundation/app.js"></script>
</body>
</html>